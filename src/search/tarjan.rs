#[derive(Clone)]
enum Color {
    WHITE,
    GRAY,
    BLACK
}

fn dfs(u: usize, g: &Vec<Vec<usize>>, color: &mut Vec<Color>, list: &mut Vec<usize>) {
    color[u] = Color::GRAY;
    for v in 0..g.len() {
        if g[u][v] != 1 {continue;}
        if let Color::WHITE = color[v] {
            dfs(v, g, color, list);
        }
    }
    color[u] = Color::BLACK;
    list.insert(0, u);
}

// グラフgに対するトポロジカルソート
// リストlistにノードの順番を記録する
fn topologicalSort(g: Vec<Vec<usize>>) -> Vec<usize> {
    let g_len = g.len();
    let mut color = vec![Color::WHITE; g_len];
    let mut list: Vec<usize> = vec![];
    
    for v in 0..g_len {
        if let Color::WHITE = color[v] {
            dfs(v, &g, &mut color, &mut list);
        }
    }
    
    list
}


fn main() {
    let g = vec![vec![0, 1, 1, 0, 1, 0],
                 vec![0, 0, 0, 0, 1, 0],
                 vec![0, 0, 0, 0, 0, 1],
                 vec![0, 0, 0, 0, 1, 0],
                 vec![0, 0, 0, 0, 0, 1],
                 vec![0, 0, 0, 0, 0, 0]
            ];
    let list = topologicalSort(g);
    println!("{:?}", list);
}