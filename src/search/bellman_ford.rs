const INF: i32 = 100000000;

// グラフgと始点s
// 負のサイクルがある場合、タプルの1つめをTrueとする
fn bellmanFord(g: Vec<Vec<i32>>, s: usize) -> (bool, Vec<Option<usize>>) {
    let g_len = g.len();
    let mut dist = vec![INF; g_len];
    let mut parent: Vec<Option<usize>> = vec![None; g_len];
    let mut is_minus = false;
    
    dist[s] = 0;
    
    for t in 0..g_len {
        for u in 0..g_len {
            if dist[u] == INF {continue;}
            for e in 0..g_len {
                if dist[e] > dist[u] + g[u][e] {
                    dist[e] = dist[u] + g[u][e];
                    parent[e] = Some(u);
                    if t == g_len-1 {
                        is_minus = true;
                    }
                }
            }
        }
    }
    
    (is_minus, parent)

}

fn main() {
    let g = vec![vec![INF,  4,  2,INF,INF],
                 vec![INF,INF,  3,  2,INF],
                 vec![INF, -1,INF,  6,  8],
                 vec![INF,INF,INF,INF, -2],
                 vec![INF,INF,INF,INF,INF]
            ];
    let (is_minus, parent) = bellmanFord(g, 0);
    println!("{},{:?}", is_minus, parent);
}