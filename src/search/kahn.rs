use std::collections::VecDeque;

// グラフgに対するトポロジカルソート
fn topologicalSort(g: Vec<Vec<usize>>) -> Vec<usize> {
    let mut que: VecDeque<usize> = VecDeque::new();
    let g_len = g.len();
    let mut deg: Vec<usize> = vec![0; g_len];
    let mut order = vec![0; g_len];
    
    // 入次数を計算
    for u in 0..g_len {
        for v in 0..g_len {
            if g[u][v] == 1 {
                deg[v] += 1;
            }
        }
    }
    
    for v in 0..g_len {
        if deg[v] == 0 {
            que.push_back(v);
        }
    }
    
    let mut t = 1;
    loop {
        if que.is_empty() {break;}
        let u = que.pop_front().unwrap();
        order[u] = t;
        t += 1;
        for v in 0..g_len {
            if g[u][v] == 0 {continue;}
            deg[v] -= 1;
            if deg[v] == 0 {
                que.push_back(v);
            }
        }
    }
    order
}


fn main() {
    let g = vec![vec![0, 1, 1, 0, 1, 0],
                 vec![0, 0, 0, 0, 1, 0],
                 vec![0, 0, 0, 0, 0, 1],
                 vec![0, 0, 0, 0, 1, 0],
                 vec![0, 0, 0, 0, 0, 1],
                 vec![0, 0, 0, 0, 0, 0]
            ];
    let order = topologicalSort(g);
    println!("{:?}", order);
}