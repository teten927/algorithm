use std::collections::BinaryHeap;
use std::cmp::Reverse;
const INF: u32 = 100000000;

// T: 最短経路木
// グラフgと始点s
fn dijkstra(g: Vec<Vec<u32>>, s: usize) -> Vec<Option<usize>> {
    let mut que = BinaryHeap::new();    // (暫定距離, ノード番号)を要素とした優先度付きキュー
    let g_len = g.len();
    let mut dist = vec![INF; g_len];
    let mut parent: Vec<Option<usize>> = vec![None; g_len];
    let mut T: Vec<usize> = vec![];
    
    dist[s] = 0;
    que.push(Reverse((0, s)));
    
    loop {
        if que.is_empty() {break;}
        
        let cost: u32;
        let u: usize;
        if let Some(Reverse((x, y))) = que.pop() {
            cost = x;
            u = y;
        } else {break;}
        
         if dist[u] < cost {continue;}
        
        T.push(u);
        
        for e in 0..g_len {
            if g[u][e] == INF {continue;}
            if T.contains(&e) {continue;}
            if dist[e] > dist[u] + g[u][e] {
                dist[e] = dist[u] + g[u][e];
                que.push(Reverse((dist[e], e)));
                parent[e] = Some(u);
            }
        }
    }
    parent
}

fn main() {
    let g = vec![vec![INF,  4,  2,INF,INF],
                 vec![  4,INF,  1,  2,INF],
                 vec![  2,  1,INF,  6,  8],
                 vec![INF,  2,  6,INF,  2],
                 vec![INF,INF,  8,  2,INF]
            ];
    let parent = dijkstra(g, 0);
    println!("{:?}", parent);
}