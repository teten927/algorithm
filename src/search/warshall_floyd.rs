const INF: i32 = 100000000;

fn warshallFloyd(g:&mut Vec<Vec<i32>>) {
    let g_len = g.len();
    for k in 0..g_len {
        for i in 0..g_len {
            for j in 0..g_len {
                if i == j {continue;}
                if g[i][j] > g[i][k] + g[k][j] {
                    g[i][j] = g[i][k] + g[k][j]
                }
            }
        }
    }

}

fn main() {
    let mut g = vec![vec![INF,  5,  1,INF],
                     vec![INF,INF,INF,  7],
                     vec![INF,  2,INF,INF],
                     vec![  3,  8,INF,INF]
                ];
    warshallFloyd(&mut g);
    println!("{:?}", g);
}