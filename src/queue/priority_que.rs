#[derive (Debug, Clone)]
struct PriorityQueue<T: Clone + Ord> {
    N: usize,       // 完全二分木のノード数
    A: Vec<T>,      // キューの要素を保持する配列
    heapSize: usize // 実際にデータを保持しているヒープサイズ
}

impl<T: Clone + Ord> PriorityQueue<T> {
    fn new() -> PriorityQueue<T> {
        PriorityQueue {
            N: 0,
            A: vec![],
            heapSize: 0
        }
    }

    fn insert(&mut self, x: T) {
        self.A.push(x);
        self.heapSize += 1;
        self.upHeap(self.heapSize - 1);
    }
    
    fn top(&self) -> T {
        self.A[0].clone()
    }
    
    fn extract(&mut self) -> T {
        let val = self.A[0].clone();
        self.A[0] = self.A[self.heapSize-1].clone();
        self.heapSize -= 1;
        self.downHeap(0);
        val
    }
    
    fn upHeap(&mut self, mut i: usize) {
        let val = self.A[i].clone();
        
        loop {
            if i == 0 {break;}
            let parent = (i+1)/2-1;
            if self.A[parent] >= val {break;}
            self.A[i] = self.A[parent].clone();
            i = parent;
        }
        
        self.A[i] = val;
    }
    
    fn downHeap(&mut self, mut i: usize) {
        let mut largest = i;
        let val = self.A[i].clone();
        
        loop {
            let left = (i+1)*2-1;
            let right = (i+1)*2;
            if left < self.heapSize && right < self.heapSize {
                if self.A[left] > self.A[right] {
                    largest = left;
                } else {
                    largest = right;
                }
            } else if left < self.heapSize {
                largest = left;
            } else if right < self.heapSize {
                largest = right;
            } else {
                break;
            }
            
            if self.A[largest] <= val {break;}
            
            self.A[i] = self.A[largest].clone();
            i = largest;
        }
        
        self.A[i] = val;
    } 
    
    
}
