// 座標の構造体
#[derive (Debug, Clone)]
struct Point {
    x: f64,
    y: f64
}

impl Point {
    // 2点間の距離の差のarctan2を求める関数
    fn diff_atan2(&self, other: &Point) -> f64 {
        (other.y - self.y).atan2(self.x - other.x)
    }
}

fn grahamScan(pg: &mut Vec<Point>) -> Vec<usize> {
    let mut lower_y = pg.iter().max_by(|a, b| a.y.partial_cmp(&b.y).unwrap()).unwrap().y; // 最も下にある点のy座標
    let mut base = pg.iter().enumerate().filter(|a| a.1.y == lower_y).min_by(|a, b| a.1.x.partial_cmp(&b.1.x).unwrap()).unwrap();   // 最も下にある点のうち、最も左にある点
    let mut st: Vec<usize> = vec![];  // 点を格納するスタック
    
    // pgをbaseを基準に偏角でソートした（インデックス、座標）の列を作成
    let mut orderd_point: Vec<(usize, Point)> = vec![]; // 列を格納する配列
    for (i, pt) in pg.iter().enumerate() {
        if i == base.0 {continue;}
        orderd_point.push((i, (*pt).clone()));
    }
    orderd_point.sort_by(|a, b| a.1.diff_atan2(&base.1).partial_cmp(&b.1.diff_atan2(&base.1)).unwrap());

    st.push(base.0);
    st.push(orderd_point[0].0);
    st.push(orderd_point[1].0);
    
    for i in 2..orderd_point.len() {
        let head = orderd_point[i].0;
        
        while st.len() >= 2 {
            let top2 = st[st.len() - 2];    // stの頂点の下の値
            let top = st[st.len() - 1];     // stの頂点の値
            // top2~top~headが時計回りならば、末尾を除去、反時計回りならば、何もしない
            if pg[top].diff_atan2(&pg[head]) < pg[top2].diff_atan2(&pg[head]) {
                st.pop();
            } else {
                break;
            }
        }
        
        st.push(head);
    }
    
    st
} 

fn main(){
    // 二次元点群
    let mut pg = vec![Point {x: 210.0, y: 100.0},
                      Point {x:  70.0, y: 120.0},
                      Point {x:  20.0, y: 100.0},
                      Point {x:  30.0, y: 180.0},
                      Point {x: 170.0, y:  40.0},
                      Point {x: 170.0, y: 100.0},
                      Point {x: 110.0, y:  80.0},
                      Point {x: 100.0, y:  30.0},
                      Point {x:  60.0, y:  50.0},
                      Point {x: 170.0, y: 145.0},
                      Point {x: 120.0, y: 120.0}];
    let plist = grahamScan(&mut pg);
    println!("{:?}", plist);
}