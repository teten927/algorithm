use std::f64::consts::PI;

const INF: f64 = 1000000000.0;

// 座標の構造体
#[derive (Debug)]
struct Point {
    x: f64,
    y: f64
}

fn giftWrapping(pg: &mut Vec<Point>) -> Vec<usize> {
    let mut lower_y = pg.iter().enumerate().max_by(|a, b| a.1.y.partial_cmp(&b.1.y).unwrap()).unwrap().0; // 最も下にある点のインデックス
    let mut plist: Vec<usize> = vec![lower_y];  // ラッピングする順に点を格納する出力用配列
    let mut prev_theta = INF;   // 一つ前の点間の偏角
    let mut best_theta: f64;    // 他点との最も大きい偏角
    let mut best_idx: Option<usize>;    // 偏角が最も大きい点のインデックス
    
    // ラッピング配列を作成する処理
    loop {
        best_theta = -INF;
        best_idx = None;
         // 起点(plistの末尾)に対し、最も反時計回りの位置にある点を求める
        for i in 0..pg.len() {
            if plist.contains(&i) {continue;}
            // 起点との偏角を求める
            let theta = (pg[plist[plist.len()-1]].y - pg[i].y).atan2(pg[i].x - pg[plist[plist.len()-1]].x);
            // 求めた偏角のうち、最も大きく、前の点間の偏角以下のものを「最も反時計回りの位置にある点」とする
            if theta >= best_theta && theta <= prev_theta {
                best_idx = Some(i);
                best_theta = theta;
            }
        }
        
        if let Some(i) = best_idx {
            let mut theta_to_start = (pg[i].y - pg[plist[0]].y).atan2(pg[plist[0]].x - pg[i].x);
            if theta_to_start == PI {
                theta_to_start = -PI;
            }
            if best_theta >= theta_to_start {
                plist.push(i);
                prev_theta = best_theta;
            } else {break;}
        } else {break;}
    }
    
    plist.push(plist[0]);
    plist
} 

fn main(){
    // 二次元点群
    let mut pg = vec![Point {x: 210.0, y: 100.0},
                      Point {x:  70.0, y: 120.0},
                      Point {x:  20.0, y: 100.0},
                      Point {x:  30.0, y: 180.0},
                      Point {x: 170.0, y:  40.0},
                      Point {x: 170.0, y: 100.0},
                      Point {x: 110.0, y:  80.0},
                      Point {x: 100.0, y:  30.0},
                      Point {x:  60.0, y:  50.0},
                      Point {x: 170.0, y: 145.0},
                      Point {x: 120.0, y: 120.0}];
    let plist = giftWrapping(&mut pg);
    println!("{:?}", plist);
}