// aとbのアドレスの値を交換する
fn swap<T: Clone>(A: &mut Vec<T>, a: usize, b: usize) {
    let tmp = A[a].clone();
    A[a] = A[b].clone();
    A[b] = tmp;
}

// 配列Aの区間[l, r]をA[r]の値を基準に分割する
fn partition (A: &mut Vec<i32>, l: usize, r: usize)-> usize  {
    let p = l;
    let mut i:i32 = (p as i32) -1;
    for j in p..r {
        if A[j] < A[r] {
            i += 1;
            swap(A, i as usize, j);
        }  
    }
    i += 1;
    swap(A, i as usize, r);
    i as usize
}

// 配列Aの区間[l, r]の要素をソート
pub fn quickSort(A: &mut Vec<i32>, l: usize, r: usize) {
    if l < r {
        let q = partition(A, l, r);
        if q>0 {quickSort(A, l, q-1);}
        if q<A.len() {quickSort(A, q+1, r);}
    }
}

fn main() {
    let mut test = vec![5,8,6,3,2,4,4,1];
    quickSort::quickSort(&mut test, 0, 7);
    println!("{:?}", test);
}