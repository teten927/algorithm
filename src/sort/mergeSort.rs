// 配列Aの区間[l, r)を反転する
fn reverse(A: &mut Vec<i32>, l: usize, r: usize) {
    for i in l..(l + (r-l)/2) {
        let j = r - (i-l) - 1;
        let tmp = A[i];
        A[i] = A[j];
        A[j] = tmp;
    }
}

// 配列Aの区間[l, m)の要素と区間[m, r)の要素をマージする
// それぞれの区間の要素は昇順に整列されている
fn merge(A: &mut Vec<i32>, l: usize, m: usize, r: usize) {
    let mut T = A.clone();
    
    reverse(&mut T, m, r);
    
    let mut i = l;
    let mut j = r-1;
    
    for k in l..r {
        if T[i] <= T[j] {
            A[k] = T[i];
            i += 1;
        } else {
            A[k] = T[j];
            j -= 1;
        }
    }
}

// 配列Aの区間[l, r)に対してマージソート
pub fn mergeSort(A: &mut Vec<i32>, l: usize, r: usize) {
    if l+1 < r {
        let m = (l+r)/2;
        mergeSort(A, l, m);
        mergeSort(A, m, r);
        merge(A, l, m, r);
    }
}

fn main() {
    let mut test = vec![5,8,6,3,2,4,4,1];
    mergeSort::mergeSort(&mut test, 0, 8);
    println!("{:?}", test);
}