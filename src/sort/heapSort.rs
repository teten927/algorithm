// aとbのアドレスの値を交換する
fn swap<T: Clone>(A: &mut Vec<T>, a: usize, b: usize) {
    let tmp = A[a].clone();
    A[a] = A[b].clone();
    A[b] = tmp;
}

// 配列Aで構築されたヒープの要素iからダウンヒープ
fn downHeap(A: &mut Vec<i32>, i: usize, N: usize) {
   let l = (i+1)*2-1;
   let r = (i+1)*2;
   let mut largest: usize;
   
    // 親（自分）、左の子、右の子の中で最大のノードを見つける
    if l < N && A[l] > A[i] {
        largest = l;
    } else {
        largest = i;
    }
    if r < N && A[r] > A[largest] {
        largest = r;
    }
    
    if largest != i {       // どちらかの子が最大の場合
        swap(A, i, largest);
        downHeap(A, largest, N);
    }
}

fn heapSort(A: &mut Vec<i32>) {
    // buildHeap
    let mut i = ((A.len()/2) as i32) - 1;
    let mut heapSize = A.len();

    loop {
        if i < 0 {break;}
        downHeap(A, i as usize, heapSize);
        i -= 1;
    }
    
    loop {
        if heapSize < 2 {break;}
        swap(A, 0, heapSize-1);
        heapSize -= 1;
        downHeap(A, 0, heapSize);
    }
    
}

fn main(){
    let mut A = vec![5,2,3,8,1,4,3,5,9,6];
    heapSort(&mut A);
    println!("{:?}", A);
}