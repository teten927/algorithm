// 間隔gを指定した挿入ソート
fn insertionSort(A: &mut Vec<i32>, g: usize) {
    for i in g..A.len() {
        let t = A[i];
        let mut j = (i as i32) - (g as i32);
        
        loop {
            if j < 0 {break;}
            if !(A[j as usize] > t) {break;}
            A[(j as usize)+g] = A[j as usize];
            j -= g as i32;
        }
        
        A[(j as usize)+g] = t;
    }
}

fn shellSort(A: &mut Vec<i32>) {
    let interval = [5, 3, 1];
    
    for g in &interval {
        insertionSort(A, *g);
    }
}

fn main(){
    let mut A = vec![5,2,3,8,1,4,3,5,9,6];
    shellSort(&mut A);
    println!("{:?}", A);
}