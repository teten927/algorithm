fn countingSort(A: &mut Vec<usize>) -> Vec<usize> {
    let mut B = vec![0; A.len()]; // 出力用の配列
    let max = (*A.iter().max().unwrap()) as usize;
    let mut C = vec![0; max+1]; // 出現数保存用の配列(配列A中の最大値分)
    let A_len = A.len();
    
    for i in 0..A_len {
        C[A[i]] += 1;
    }
    
    for i in 1..C.len() {
        C[i] += C[i-1] 
    }
    
    for i in 0..A_len {
        let index = A_len-i-1;
        C[A[index]] -= 1;
        B[C[A[index]]] = A[index];
    }
    
    B
}

fn main(){
    let mut A = vec![5,2,3,8,1,4,3,5,9,6];
    let B = countingSort(&mut A);
    println!("{:?}", B);
}