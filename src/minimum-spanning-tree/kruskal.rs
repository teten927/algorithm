const INF: i32 = 1000000000;

// union-find木
// サイズNの森をベースとした互いに素な集合
struct DisjointSet {
    parent: Vec<usize>,  // 森を構成する各ノードの親を保持する配列
    rank: Vec<u32>       // rankを管理する配列
}

impl DisjointSet {
    fn new(N: &usize) -> DisjointSet {
        let mut tmp_vec: Vec<usize> = vec![];
        for i in 0..(*N) {
            tmp_vec.push(i);
        }
        DisjointSet {
            parent: tmp_vec,
            rank: vec![0; *N]
        }
    }
    
    fn unite(&mut self, x: usize, y: usize) {
        let root1 = self.findSet(x);
        let root2 = self.findSet(y);
        self.link(root1, root2);
    }
    
    fn findSet(&mut self, x: usize) -> usize {
        if self.parent[x] != x {
            self.parent[x] = self.findSet(self.parent[x]);
        }
        self.parent[x]
    }
    
    fn link(&mut self, x: usize, y: usize) {
        if self.rank[x] > self.rank[y] {
            self.parent[y] = x;
        } else {
            self.parent[x] = y;
            if self.rank[x] == self.rank[y] {
                self.rank[y] += 1;
            }
        }
    }
}

#[derive (Debug)]
struct Edge {
    u: usize,   // 1つめの端点
    v: usize,   // 2つめの端点
    weight: i32 // 重み
}

// グラフgから最小全域木MSTを構築する
fn kruskal(g: Vec<Vec<i32>>) -> Vec<Edge> {
    let g_len = g.len();
    let mut MST: Vec<Edge> = vec![];
    let mut edges: Vec<Edge> = vec![];
    
    for i in 0..g_len {
        for j in 0..g_len {
            if g[i][j] != INF {
                   edges.push(Edge {
                       u: i,
                       v: j,
                       weight: g[i][j]
                   })
           }
        }
    }
    
    // edgesを重みの昇順で整列する
    edges.sort_by(|a, b| a.weight.cmp(&b.weight));
    
    let mut ds = DisjointSet::new(&g_len);   // 要素数g_lenの互いに素な集合を生成する
    
    for e in edges {
        let u = e.u;
        let v = e.v;
        
        if ds.findSet(u) != ds.findSet(v) {
            ds.unite(u, v);
            MST.push(e);
        }
    }
    
    MST
}

fn main() {
    let g = vec![vec![INF,  3,  1,  9, 11,INF,INF],
                 vec![  3,INF,  1,INF,INF,  4,INF],
                 vec![  1,  1,INF,INF,  2,  8,  7],
                 vec![  9,INF,INF,INF,  2,INF,INF],
                 vec![ 11,INF,  2,  2,INF,INF,  3],
                 vec![INF,  4,  8,INF,INF,INF,  1],
                 vec![INF,INF,  7,INF,  3,  1,INF]
            ];
    let MST = kruskal(g);
    println!("{:?}", MST);
}