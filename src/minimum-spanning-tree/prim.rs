const INF: i32 = 1000000000;

#[derive (Debug, Clone)]
enum parent {
    node(usize),
    NIL    
}

// グラフgの最小全域木を求める
// T: 最小全域木に含まれるノードの集合
fn prim(g: Vec<Vec<i32>>) -> Vec<parent>{
    let s = 0;  // 適当な始点を決める
    let g_len = g.len();
    let mut dist = vec![INF; g_len];
    let mut parent = vec![parent::NIL; g_len];
    let mut T: Vec<usize> = vec![];
    
    dist[s] = 0;
    
    loop {
        let mut u = parent::NIL;
        let mut minv = INF;
        // find minimum
        for v in 0..g_len {
            if T.contains(&v) {continue;}
            if dist[v] < minv {
                u = parent::node(v);
                minv = dist[v];
            }
        }
        
        let mut u_val: usize;
        
        match u {
            parent::NIL => break,
            parent::node(x) => {
                T.push(x);
                u_val = x;
            }
        }
        
        for v in 0..g_len {
            if g[u_val][v] == INF {continue;}
            if T.contains(&v) {continue;}
            if dist[v] > g[u_val][v] {
                dist[v] = g[u_val][v];
                parent[v] = parent::node(u_val);
            }
        }
    }
    parent
    
}

fn main() {
    let g = vec![vec![INF,  4,  2,  9, 11,INF,INF],
                 vec![  4,INF,  1,INF,INF,  4,INF],
                 vec![  2,  1,INF,INF,  2,  8,  7],
                 vec![  9,INF,INF,INF,  2,INF,INF],
                 vec![ 11,INF,  2,  2,INF,INF,  3],
                 vec![INF,  4,  8,INF,INF,INF,  1],
                 vec![INF,INF,  7,INF,  3,  1,INF]
            ];
    let list = prim(g);
    println!("{:?}",list);
}