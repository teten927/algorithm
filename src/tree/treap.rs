#[derive (Debug, Clone)]
struct Node {
    left: Option<Box<Node>>,
    right: Option<Box<Node>>,
    key: usize,
    pri: u32
}

// 木構造生成用関数（テストデータ作成用）
impl Node {
    fn new(key: usize, pri: u32) -> Node {
        Node {
            left: None,
            right: None,
            key: key,
            pri: pri
        }
    }
    
    // 再帰的にノードを探索
    fn search_insert(&mut self, key: usize, pri: u32, mut flags: Vec<usize>) -> bool {
        if flags.pop().unwrap() == 0 {
            if let Some(x) = &mut self.left {
                (*x).search_insert(key, pri, flags)
            } else if flags.is_empty(){
                self.left = Some(Box::new(Node::new(key, pri)));
                true
            } else {
                false
            }
        } else {
             if let Some(x) = &mut self.right {
                (*x).search_insert(key, pri, flags)
            } else if flags.is_empty(){
                self.right = Some(Box::new(Node::new(key, pri)));
                true
            }  else {
                false
            }
        }
    }
    
    // positionで指定された位置に(key, pri)ノードを追加する
    fn insert(&mut self, mut position: usize, key: usize, pri: u32) -> bool {
        let mut flags: Vec<usize> = vec![];
        loop {
            if position == 1 {break;}
            flags.push(position % 2);
            position /= 2;
        }
        self.search_insert(key, pri, flags)
    }
}

struct Treap {
    root: Option<Box<Node>>
}

impl Treap {
    fn rightRotate(t: &mut Node) {
        let mut s: Node = Node::new(0,0); 
        if let Some(x) = &mut t.left {
            s = *(x.clone());
        }
        t.left = s.right;
        s.right = Some(Box::new(t.clone()));
        *t = s;
    }
    
    fn leftRotate(t: &mut Node) {
        let mut s: Node = Node::new(0,0); 
        if let Some(x) = &mut t.right {
            s = *(x.clone());
        }
        t.right = s.left;
        s.left = Some(Box::new(t.clone()));
        *t = s;
    }

    // 再帰的に挿入位置を検索
    fn search_insert(t: &mut Node, key: usize, pri: u32) {
        // 重複するキーを無視する
        if key == t.key {
            return;
        }
        
        if key < t.key {    // 左の子に移動
            if let Some(x) = &mut t.left {
                Treap::search_insert(&mut *x, key, pri);
            } else {
                t.left = Some(Box::new(Node::new(key, pri)));
            }
            // その子の優先度が高ければ、右回転で持ち上げる
            if let Some(y) = &mut t.left {
                if t.pri < (*y).pri {
                    Treap::rightRotate(t);
                }
            }
        } else {    // 右の子に移動
            if let Some(x) = &mut t.right {
                Treap::search_insert(&mut *x, key, pri);
            } else {
                t.right = Some(Box::new(Node::new(key, pri)));
            }
            // その子の優先度が高ければ、左回転で持ち上げる
            if let Some(y) = &mut t.right {
                if t.pri < (*y).pri {
                    Treap::leftRotate(t);
                }
            }
        }
    }
    
    fn insert(&mut self, key: usize, pri: u32) {
        if let Some(x) = &mut self.root {
            Treap::search_insert(&mut *x, key, pri);
        } else {
            self.root = Some(Box::new(Node::new(key, pri)));
        }
    }
    
    // 対象を再帰的に探索
    fn search_erase(t: &mut Node, key: usize) -> bool {
        // tが削除対象
        if key == t.key {
            if let (None, None) = (&mut t.left, &mut t.right) {       // tが葉
                return true;
            }
            if let (None, Some(_)) = (&mut t.left, &mut t.right) {    // tがただ1つの右の子を持つ
                Treap::leftRotate(t);
                Treap::search_erase(t, key);
                return false;
            }
            if let (Some(_), None) = (&mut t.left, &mut t.right) {    // tがただ1つの左の子を持つ
                Treap::rightRotate(t);
                Treap::search_erase(t, key);
                return false;
            }                                 
            if let (Some(x), Some(y)) = (&mut t.left, &mut t.right) {    // tが2つの子を持つ
                // 優先度が高い子を持ち上げる
                if (*x).pri > (*y).pri {
                    Treap::rightRotate(t);
                } else {
                    Treap::leftRotate(t);
                }
                Treap::search_erase(t, key);
                return false;
            }
        }
        
        if key < t.key {    // 左の子に移動
            if let Some(x) = &mut t.left {
                if Treap::search_erase(&mut *x, key) {
                    t.left = None;
                };
            }
        } else {    // 右の子に移動
            if let Some(x) = &mut t.right {
                if Treap::search_erase(&mut *x, key) {
                    t.right = None;
                }
            }
        }
        false
    }
    
    fn erase(&mut self, key: usize) {
        if let Some(x) = &mut self.root {
            Treap::search_erase(&mut *x, key);
        }
    }
}

fn main() {
    let mut node = Node::new(35, 99);
    node.insert(2, 3, 80);
    node.insert(3, 80, 76);
    node.insert(4, 1, 53);
    node.insert(5, 14, 25);
    node.insert(6, 42, 3);
    node.insert(7, 86, 47);
    node.insert(10, 7, 10);
    node.insert(11, 21, 12);
    let mut treap = Treap {root: Some(Box::new(node))};
    treap.insert(6,90);
    treap.erase(35);
    println!("{:?}", treap.root);
}